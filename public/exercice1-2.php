<?php

interface villeInterface
{
    public function __construct(string $name, string $department);

    public function getInfo(): string;

    public function getName(): string;
}


interface trainInterface
{
    public function __construct(int $identifier, int $places, ville $from, ville $to);

    public function getPlaces(): int;

    public function getInfo(): string;

    public function decreaseOnePlace(): void;
}

interface humanInterface
{
    public function __construct(string $name);

    public function getName(): string;

    public function reservation(train $train): void;
}


class ville implements villeInterface
{
    private $name;
    private $department;

    public function __construct(string $name, string $department)
    {
        $this->name = $name;
        $this->department = $department;
    }

    public function getInfo(): string
    {
        return "La ville de {$this->name} est dans le département : {$this->department}\n";
    }

    public function getName(): string
    {
        return $this->name;
    }
}

class train implements trainInterface
{
    private $identifier;
    private $places;
    private $from;
    private $to;

    public function __construct(int $identifier, int $places, ville $from, ville $to)
    {
        $this->identifier = $identifier;
        $this->places = $places;
        $this->from = $from->getName();
        $this->to = $to->getName();
    }

    public function getPlaces(): int
    {
        return $this->places;
    }

    public function getInfo(): string
    {
        return "Le train {$this->identifier} en provenance de {$this->from} en direction de {$this->to} dispose de {$this->places} places disponibles. \n";
    }

    public function decreaseOnePlace(): void
    {
        --$this->places;
    }

}

class human implements humanInterface
{
    private $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function reservation(train $train): void
    {
        $train->decreaseOnePlace();
    }
}

$ville1 = new ville('Limoges', 'Haute-Vienne');
$ville2 = new ville('La rochelle', 'Charente-Maritime');
echo $ville1->getInfo();
echo $ville2->getInfo();

$train = new train(1234, 120, $ville1, $ville2);

$john = new human('John');

echo $train->getInfo();
$john->reservation($train);
echo $train->getInfo();

?>
