<?php

namespace LimoDeals;

interface EntityInterface
{
    public function getCreationDate();
}

class Deal implements EntityInterface
{
    public $name;
    public $description;
    public $createdAt;

    public function __construct($name) {
        $this->name = $name;
        $this->createdAt = new \DateTime();
    }

    public function getCreationDate() {
        return $this->createdAt;
    }
}

class Category implements EntityInterface
{
    public $name;
    public $createdAt;
    public $deals;

    public function __construct($name) {
        $this->name = $name;
        $this->deals = [];
        $this->createdAt = new \DateTime();
    }

    /** @var \LimoDeals\Deal $deal */
    public function addDeal($deal) {
        $this->deals[] = $deal;
    }

    public function getCreationDate() {
        return $this->createdAt;
    }
}

$deal1 = new Deal('First Deal');
$deal2 = new Deal('Other Stuff');

/** @var \LimoDeals\Category $category */
$category = new Category('High tech');
$category->addDeal($deal1);
$category->addDeal($deal2);
var_dump("<pre>", $category);