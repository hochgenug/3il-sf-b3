<?php

namespace App\Controller;

use App\Entity\Deal;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DealController extends AbstractController
{
    /**
     * List the deals.
     *
     * @Route("/deal/list", name="deal_list", methods="GET")
     * @Route("/", name="homepage", methods="GET")
     *
     * @return Response
     */
    public function list(): Response
    {
        return new Response("This page is for the list of the deals.");
    }

    /**
     * Show a specific deal.
     *
     * @Route("/deal/show/{dealId}", name="deal_show", requirements={"dealId": "\d+" }, methods="GET")
     *
     * @param int $dealId
     * @return Response
     */
    public function show(int $dealId): Response
    {
        return new Response("Showing the Deal : {$dealId}");
    }

    /**
     * Toggle the enable status between true & false.
     *
     * @Route("/deal/toggle/{dealId}", name="toggle", requirements={"dealId": "\d+" }, methods="GET")
     *
     * @param int $dealId
     * @return Response
     */
    public function toggleEnableAction(int $dealId): Response
    {
        $em = $this->getDoctrine()->getManager();
        /** @var Deal $deal */
        $deal = $em->getRepository('App:Deal')->find($dealId);

        if (!$deal) {
            throw $this->createNotFoundException(
                'No deal found for id ' . $dealId
            );
        }

        $deal->setEnable(!$deal->getEnable());

        $em->flush();

        return new Response("Enable for the dealId {$dealId} is now set to : " . (int)$deal->getEnable());
    }
}
