<?php

namespace App\DataFixtures;

use App\Entity\Deal;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class DealFixtures extends Fixture
{
    /**
     * Load the deal fixtures.
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        $deals = [
            [
                'name'        => 'Commode',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ultricies tellus sed sollicitudin elementum. Mauris ac velit iaculis, fringilla tortor in, imperdiet nunc. Quisque at massa mattis, malesuada nisl quis, malesuada turpis. Curabitur quis quam eleifend, sodales risus at, ullamcorper sem. Quisque vel ex vitae est feugiat posuere nec a eros. Quisque pulvinar pulvinar ligula eget consectetur. Maecenas pretium dapibus urna. Nullam placerat imperdiet augue, eleifend fermentum velit eleifend in. Quisque lacinia tortor massa, sit amet lacinia lectus eleifend ac. Quisque maximus, est id rutrum ultricies, lectus est molestie nisl, in sagittis dui risus pretium felis. Morbi maximus ex sed leo placerat, ut convallis tortor molestie. Vivamus non elit vitae ante elementum scelerisque a sed leo. Nullam tempor orci sed turpis semper accumsan. Aenean suscipit efficitur tincidunt. Maecenas vel lobortis ipsum, ut consequat purus. Proin sed diam vel ex elementum tincidunt.',
                'price'       => '15.50',
                'category'    => CategoryFixtures::CATEGORIES[0],
                'enable'      => false
            ],
            [
                'name'        => 'Buffet',
                'description' => 'Etiam fermentum ultricies augue, vitae iaculis tortor dictum nec. Vestibulum mattis ante ac magna malesuada vulputate. Curabitur ut fringilla turpis. Phasellus finibus ante sit amet auctor eleifend. Aliquam neque nibh, gravida vel tellus in, dignissim laoreet justo. Fusce eget elit enim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed in est sed ante aliquam semper. In accumsan vel erat hendrerit iaculis. Nunc aliquet pharetra tellus eget congue. In in volutpat dolor. Aliquam ultrices ex sit amet enim pretium interdum. Curabitur vel nisl accumsan, semper mauris feugiat, bibendum urna. Maecenas ac vehicula urna. Aenean feugiat dapibus nisl sed gravida. Vestibulum iaculis quam ac lacus semper, at dictum quam vehicula.',
                'price'       => '18',
                'category'    => CategoryFixtures::CATEGORIES[0],
                'enable'      => true
            ],
            [
                'name'        => 'Iphone 5',
                'description' => 'Nullam a turpis felis. Proin mattis eu ipsum scelerisque ultrices. Proin hendrerit nisl tortor, at fringilla lacus efficitur et. Sed non diam tristique, venenatis lectus scelerisque, eleifend leo. Praesent malesuada, lectus ut tempus volutpat, turpis ligula pulvinar libero, vel venenatis lorem nunc scelerisque nisi. Nullam nec ex a lorem pellentesque posuere. Morbi consectetur risus ac turpis efficitur pharetra. Aliquam leo orci, scelerisque ut leo quis, placerat ullamcorper quam.',
                'price'       => '25',
                'category'    => CategoryFixtures::CATEGORIES[1],
                'enable'      => true
            ],
            [
                'name'        => 'Galaxy S10',
                'description' => 'Proin bibendum elit sed lorem elementum, eu faucibus quam consequat. Ut scelerisque odio in odio eleifend pulvinar. Mauris viverra elit tellus, at sagittis ex vehicula ut. Etiam aliquet tellus est, vitae tincidunt orci fringilla quis. Nulla scelerisque ut massa nec pharetra. Vestibulum pellentesque quam at augue cursus viverra. Cras consectetur dignissim nisl in bibendum. Vestibulum facilisis ultricies ligula ut semper. Duis ut imperdiet elit. Vestibulum at leo tincidunt, gravida mi id, blandit enim.',
                'price'       => '300',
                'category'    => CategoryFixtures::CATEGORIES[1],
                'enable'      => true
            ]
        ];

        foreach ($deals as $deal) {
            $dealEntity = $this->initDeal($deal);
            $manager->persist($dealEntity);
        }

        $manager->flush();
    }

    /**
     * Initialize one Deal.
     *
     * @param array $deal
     *
     * @return Deal
     */
    private function initDeal(array $deal): Deal
    {
        $dealEntity = new Deal();
        $dealEntity->setName($deal['name']);
        $dealEntity->setDescription($deal['description']);
        $dealEntity->setPrice($deal['price']);
        $dealEntity->setEnable($deal['enable']);
        if (\in_array($deal['category'], CategoryFixtures::CATEGORIES, true)) {
            $dealEntity->addCategory($this->getReference($deal['category']));
        }

        return $dealEntity;
    }
}
