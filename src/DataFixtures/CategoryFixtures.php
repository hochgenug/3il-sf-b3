<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

/**
 * Class CategoryFixtures
 */
class CategoryFixtures extends Fixture
{
    public const CATEGORIES = ['Meubles', 'High-tech'];

    /**
     * Load the category fixtures.
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        foreach (self::CATEGORIES as $category) {
            $categoryEntity = $this->initCategory($category);
            $manager->persist($categoryEntity);
            $this->addReference($category, $categoryEntity);
        }
        $manager->flush();
    }

    /**
     * Initialize the Category.
     *
     * @param string $name
     *
     * @return Category
     */
    private function initCategory(string $name): Category
    {
        $category = new Category();
        $category->setName($name);

        return $category;
    }
}
